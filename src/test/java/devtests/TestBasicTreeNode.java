/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package devtests;

import org.junit.Assert;
import org.junit.Test;

import edu.semeru.android.gcat.tree_builder.BasicTreeNode;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author georgewpurnell
 */
public class TestBasicTreeNode {

    /**
     *
     */
    @Test
    public void TestDSNodeEquality1() {
        BasicTreeNode node1 = new BasicTreeNode(10, 20, 30, 40);
        BasicTreeNode node2 = new BasicTreeNode(10, 20, 30, 40);
        Assert.assertTrue(node1.equals(node2));
    }

    /**
     *
     */
    @Test
    public void TestDSNodeEquality2() {
        BasicTreeNode node1 = new BasicTreeNode(12, 20, 30, 40);
        BasicTreeNode node2 = new BasicTreeNode(10, 20, 33, 40);
        Assert.assertFalse(node1.equals(node2));
    }

    /**
     *
     */
    @Test
    public void TestDSNodeEquality3() {
        BasicTreeNode node1 = new BasicTreeNode(0, 1000, 0, 40);
        BasicTreeNode node2 = new BasicTreeNode(0, 1000, 0, 40);
        Assert.assertTrue(node1.equals(node2));
    }

    /**
     *
     */
    @Test
    public void TestDSNodeEquality4() {
        BasicTreeNode node1 = new BasicTreeNode(10, 20, 30, 40);
        BasicTreeNode node2 = new BasicTreeNode(10, 25, 30, 40);
        Assert.assertFalse(node1.equals(node2));
    }
}

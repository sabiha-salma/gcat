/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package acceptancetests.yelp;

import java.io.IOException;
import java.util.List;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.semeru.android.gcat.changes.Change;
import edu.semeru.android.gcat.changes.ChangeType;
import edu.semeru.android.gcat.changes.MissingComponentChange;
import edu.semeru.android.gcat.matching.MatchingAnalysis;

/**
 *
 * @author shuningchen
 */
public class Test1_14 {
    private static YelpTest test;
    private static MatchingAnalysis checker;
    
    /**
     *
     * @throws IOException
     */
    @BeforeClass
    public static void setUpClass() throws IOException {
        test = new YelpTest();
        test.defaultTestSetup("com.yelp.android-original1-14.png", "com.yelp.android-ui-dump-1-14.xml");
        test.runMatchingAnalysis("1-14");
        checker = test.checker;
        System.out.println("-----------Beginning Tests for Yelp 1-14-----------");
    }
    
    /**
     *
     */
    @Test
    public void testComponentSizeViolationQuantity() {
        List<Change> violations = checker.getViolationsOfType(ChangeType.COMPONENT_SIZE);
        System.out.println("Number of Component Size Violations: " + violations.size() + " expecting: " + "0");
        assertTrue(violations.isEmpty());
    }
    
    /**
     *
     */
    @Test
    public void testLocationChangeQuantity() {
        List<Change> violations = checker.getViolationsOfType(ChangeType.LOCATION_CHANGE);
        System.out.println("Number of Location Change Violations: " + violations.size() + " expecting: 1");
        assertTrue(violations.size() == 1);
    }
    
    /**
     *
     */
    @Test
    public void testMissingComponentViolationQuantity() {
        List<MissingComponentChange> violations = checker.getMissingComponents();
        System.out.println("Number of Missing Component Violations: " + violations.size() + " expecting: 0");
        for(int i = 0; i < violations.size(); i ++){
            System.out.println("Missing node:" + violations.get(i).getNewNode().printOutGUICheckerFormat());     
        }
        assertTrue(violations.isEmpty());
    }
    
    /**
     *
     */
    @Test
    public void testColorMismatchViolationsQuantity() {
        List<Change> violations = checker.getViolationsOfType(ChangeType.COLOR_MISMATCH);
        System.out.println("Number of Color Mismatch Violations: " + violations.size() + " expecting: 0");
        assertTrue(violations.isEmpty());
    }
    
    /**
     *
     */
    @Test
    public void testTextMismatchViolationQuantity() {
        List<Change> violations = checker.getViolationsOfType(ChangeType.Text_MISMATCH);
        System.out.println("Number of Text Mismatch Violations: " + violations.size() + " expecting: 1");
        assertTrue(violations.size() == 1);
    }

    /**
     *
     */
    @Test
    public void testExtraComponentViolationQuantity() {
        List<Change> violations = checker.getViolationsOfType(ChangeType.Extra_Component);
        System.out.println("Number of Extra Component Violations: " + violations.size() + " expecting: 0");
        assertTrue(violations.isEmpty());
    }
    
    /**
     *
     */
    @Test
    public void testFontMismatchViolationQuantity() {
        List<Change> violations = checker.getViolationsOfType(ChangeType.Font_MISMATCH);
        System.out.println("Number of Font Mismatch Violations: " + violations.size() + " expecting: 0");
        assertTrue(violations.isEmpty());
    }
    
    /**
     *
     */
    @Test
    public void testTextColorViolationQuantity() {
        List<Change> violations = checker.getViolationsOfType(ChangeType.TEXT_COLOR);
        System.out.println("Number of Text Color Violations: " + violations.size() + " expecting: 0");
        assertTrue(violations.isEmpty());
    }
    
    /**
     *
     */
    @Test
    public void testImageDifferenceViolationQuantity() {
        List<Change> violations = checker.getViolationsOfType(ChangeType.Image_DIFF);
        System.out.println("Number of Image Difference Violations: " + violations.size() + " expecting: 0");
        for(int i = 0; i < violations.size(); i ++){
            System.out.println("ImageDiff node:" + violations.get(i).getNewNode().printOutGUICheckerFormat());     
        }
        assertTrue(violations.isEmpty());
    }
}

/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.nlgeneration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import edu.semeru.android.gcat.changes.Change;
import edu.semeru.android.gcat.changes.ChangeType;
import edu.semeru.android.gcat.tree_builder.BasicTreeNode;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 * Encapsulates all the information needed to generate the HTML report.
 * @author cdestes
 */
public class ReportContainer {

    private String oldScreenshotPath = ""; // Old, pre-change GUI screenshot
    private String newScreenshotPath = ""; // New, post-change GUI screenshot
    private String combinedScreenshotPath = ""; // Image with changes encirculed in a red box.
    private String html =  ""; // The row HTML report
    private String summary = ""; // The natural language summary of changes.
    private ArrayList<GUIChange> changes; // A list of all the changes, optimized for use in the html.
    private HashMap violationCounter; // A counter with all violation types, and a count of their occurrences.
    
    /**
     * String paths for html template and output
     */
    private String htmlSummaryTemplatePath = "html" + File.separator + 
            "res" + File.separator + "templates" + File.separator + 
            "summaryTemplate.html";
    private String htmlSummaryPath = "html" + File.separator + "Full-Report.html";

    /**
     * Constructor
     */
    public ReportContainer() {
        changes = new ArrayList<GUIChange>();
        violationCounter = new HashMap<ChangeType, Integer>();
        
        for( ChangeType v: ChangeType.values()){
            violationCounter.put(v, 0);            
        }
        try {
            html = new Scanner(new File(htmlSummaryTemplatePath)).useDelimiter("\\Z").next();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Set the natural language summary.
     * @param summary Natural language summary of changes
     */
    public void setSummary(String summary){
        this.summary = summary;
    }

    /**
     * Get the list of all changes.
     * @return list of changes
     */
    public ArrayList<GUIChange> getChangeList() {
        return this.changes;
    }
    
    /**
     * Convert a violation into a GUIChange object, and add it to the list of changes. 
     * Also increments the count of the type of the violation.
     * @param violation 
     */
    public void addChange(Change violation) {
        String naturalLanguageDescription = violation.toString();
        String oldImagePath = ""; // These should point to some kind of "Not applicable" image
        String newImagePath = "";
        
        // These ifs prevent null pointer exceptions.
        UiTreeNode oldNode = violation.getOldNode();
        if (oldNode != null)
            oldImagePath = oldNode.getCroppedImagePath();
        
        UiTreeNode newNode = violation.getNewNode();
        if (newNode != null)
            newImagePath = newNode.getCroppedImagePath();

        GUIChange guiChange = new GUIChange(naturalLanguageDescription, oldImagePath, newImagePath);

        this.changes.add(guiChange);
        
        int count = (int) violationCounter.get(violation.type);
        violationCounter.replace(violation.type, ++count);
    }

    /**
     * Add a change to the HTML report.
     * @param changes
     */
    public void makeHtmlFiles(GUIChange changes) {
        String newChangeString= "<dt>"
            + "<a href = \"#accordion1\" aria-expanded = \"false\" aria-controls = \"accordion1\" class=\"accordion-title accordionTitle js-accordionTrigger\">"
            + changes.getNaturalLanguageDescription()    + "</a> </dt> "
            + "<dd class=\"accordion-content accordionItem is-collapsed\" id = \"accordion1\" aria-hidden = \"true\">" 
            + "<div style=\"display: flex;\"></div>"
            + "<img class=\"croppedImage\" src=\"$previousComponentImg\"></img><img class=\"croppedImage\" src=\"$newComponentImg\"></img></dd>$newComponent";

        PrintWriter writer = null;
        try {
            String currReportPath = htmlSummaryPath;
            writer = new PrintWriter(currReportPath , "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportContainer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ReportContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        writer.println(html);
        
        html = html.replace("$newComponent", newChangeString);
        html = html.replace("$previousComponentImg", changes.getOldCroppedImagePath());
        html = html.replace("$newComponentImg", changes.getNewCroppedImagePath());
        writer.println(html);
        writer.close();
    }

    /**
     * Generate the HTML report.
     */
    public void printAllChanges() {
        System.out.println();
        System.out.println("List of violation changes:");
        html = html.replace("$previousScreenshot", oldScreenshotPath);
        html = html.replace("$changesScreenshot", combinedScreenshotPath);
        html = html.replace("$newScreenshot", newScreenshotPath);
        for (GUIChange curr : this.changes) {
            System.out.println("   * " + curr.getNaturalLanguageDescription());
            makeHtmlFiles(curr);
            
        }
        
        // Remove last $newComponent
        html = html.replace("$newComponent", "");
        PrintWriter writer  = null;
        try {
            String currReportPath = htmlSummaryPath;
            writer = new PrintWriter(currReportPath , "UTF-8");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReportContainer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ReportContainer.class.getName()).log(Level.SEVERE, null, ex);
        }
        html = html.replace("$summary", this.summary);
        html = html.replace("$tree", BasicTreeNode.getStoredCommonTree());
        writer.println(html);
        writer.close();
    }
    
    /**
     * Set the path to the screenshot of the old, pre-change GUI screenshot.
     * @param oldScreenshotPath screenshot path to set
     */
    public void setOldScreenshotPath(String oldScreenshotPath) {
        this.oldScreenshotPath = oldScreenshotPath;
    }

    /**
     * Set the path to the screenshot of the new, post-change GUI screenshot.
     * @param newScreenshotPath screenshot path to set
     */
    public void setNewScreenshotPath(String newScreenshotPath) {
        this.newScreenshotPath = newScreenshotPath;
    }
    
    /**
     * Set the path to the screenshot with the red change bounding boxes drawn on it.
     * @param newScreenshotPath screenshotpath to set
     */
    public void setCombinedScreenshotPath(String newScreenshotPath) {
        this.combinedScreenshotPath = newScreenshotPath;
    }
    
    /**
     * Get the map that counts the number of times various violation types occur.
     * @return violation counts
     */
    public HashMap getViolationCounter(){
        return this.violationCounter;
    }
    
}

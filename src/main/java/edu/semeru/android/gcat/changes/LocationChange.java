/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.changes;

import edu.semeru.android.gcat.tree_builder.ConstantSettings;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 * Violation that describes the case of when a component changes location.
 * @author shuningchen
 */

public class LocationChange extends Change {

    private int newx;
    private int newy;
    private int prevx;
    private int prevy;


    /**
     * Construct violation storing the location info of both prev and new release
     * @param uiNode node from new commit with different location
     * @param dsNode node from old commit with different location
     */
    public LocationChange(UiTreeNode uiNode, UiTreeNode dsNode) {
        super(ChangeType.LOCATION_CHANGE);;
        super.newNode = uiNode;
        super.oldNode = dsNode;
        this.newx = uiNode.getX();
        this.newy = uiNode.getY();
        this.prevx = dsNode.getX();
        this.prevy = dsNode.getY();  
    }

    /**
     * True if the distance of the location change is greater than the
     * violation threshold.
     * 
     * @return boolean
     */
    @Override
    public boolean isExceededThreshold() {
        ConstantSettings settings = ConstantSettings.getInstance();
        int xDis = Math.abs(prevx - newx);
        int yDis = Math.abs(prevy - newy);
        return Math.sqrt(xDis * xDis + yDis * yDis) > settings.getViolationThreshold();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Location Change : ");
        if (Math.abs(prevx - newx) > 0){
            sb. append("the distance between the left edge of the component and the left edge of the screen is " + newx + " px");
            sb.append(" while it used to be " + prevx + " px.");
        } 
        if (Math.abs(prevy - newy) > 0){
            sb. append("the distance between the top of the component and the top of the screen is " + newy + " px");
            sb.append(" while it used to be " + prevy + " px.");
        }
        return sb.toString();
    }

}
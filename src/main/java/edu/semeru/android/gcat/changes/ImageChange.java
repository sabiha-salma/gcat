/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.changes;

import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 * Violation that describes the case when an image (ImageButton, ImageView) on the screen changes. 
 *
 */
public class ImageChange extends Change {

    /**
     * The missing node in design
     */
    UiTreeNode imageDiffNode = null;
    boolean areHistogramsClose = false;
    boolean areImagesSame = false;
    float diffRatio = 0;

    /**
     *
     * @param imageDiffNode old node with different image
     * @param uiNode new node with different image
     * @param diffRatio difference ratio from PID analysis
     * @param areHistogramsClose boolean if colors are similar
     * @param areImagesSame boolean if images are the same with different locations
     */
    public ImageChange(UiTreeNode imageDiffNode, UiTreeNode uiNode, float diffRatio, boolean areHistogramsClose, boolean areImagesSame) {
        super(ChangeType.Image_DIFF);
        super.newNode = uiNode;
        this.imageDiffNode = imageDiffNode;
        this.diffRatio = diffRatio;
        this.areImagesSame = areImagesSame;
        this.areHistogramsClose = areHistogramsClose;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("The "+ newNode.getType() + " at location " + "(" + newNode.getX() + "," + newNode.getY() + ") image has");
        if (!areImagesSame) {
            sb.append(" a totally different image.");
        } else {
            sb.append(" changed color.");
        }
        return sb.toString();
    }

    /**
     *
     * @return true
     */
    @Override
    public boolean isExceededThreshold() {
        return true;
    }

    /* public UiTreeNode getImageDiffNode(){
     * return imageDiffNode;
     * }
     * public boolean getAreHistogramsClose(){
     * return areHistogramsClose;
     * }*/
}

/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.tree_builder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This class stores constant settings
 *
 * @author Boyang Li
 *
 */
public class ConstantSettings {

    /**
     * Provided by HUAWEI. 
     * Non-Changeable by parameters since since we consider it as an experience number. 
     */
    public final static int MATCHINGRATIO = 4;

    private static ConstantSettings instance = null;

    /**
     * Retrieve instance of CostantSettings
     * 
     * @return instance of ConstantSettings
     */
    public static ConstantSettings getInstance() {
        if (instance == null) {
            instance = new ConstantSettings();
        }
        return instance;
    }

    private ConstantSettings() {
        //Constructor
    }

    @SerializedName("IgnoredCompDesign")
    @Expose
    private int[][] ignoredCompDesign = null;
    @SerializedName("UIBoard")
    @Expose
    private int[] uIBoard = null;
    @SerializedName("DSBoard")
    @Expose
    private int[] dSBoard = null;
    @SerializedName("ViolationThreshold")
    @Expose
    private int violationThreshold;
    @SerializedName("ImgDiffThreshold")
    @Expose
    private int ImgDiffThreshold;


    /**
     * Retrieve ignoredCompDesign
     * 
     * @return the ignoredCompDesign
     */
    public int[][] getIgnoredCompDesign() {
        return ignoredCompDesign;
    }

    /**
     * Set ignoredCompDesign
     * 
     * @param ignoredCompDesign the ignoredCompDesign to set
     */
    public void setIgnoredCompDesign(int[][] ignoredCompDesign) {
        this.ignoredCompDesign = ignoredCompDesign;
    }

    /**
     * Retrieve new commit board
     * @return the new commit board
     */
    public int[] getUIBoard() {
        return uIBoard;
    }

    /**
     * Set the new commit board
     * @param uIBoard the new commit board to set
     */
    public void setUIBoard(int[] uIBoard) {
        this.uIBoard = uIBoard;
    }

    /**
     * Retrieve the old commit board
     * 
     * @return the old commit board
     */
    public int[] getDSBoard() {
        return dSBoard;
    }

    /**
     * Set the old commit board
     * @param dSBoard the old commit board to set
     */
    public void setDSBoard(int[] dSBoard) {
        this.dSBoard = dSBoard;
    }

    /**
     * Retrieve the thresholdMatchDistance
     * 
     * @return The thresholdMatchDistance
     */
    public int getThresholdMatchDistance() {
        //return thresholdMatchDistance;
        return (uIBoard[2] + uIBoard[3]) / 2 / MATCHINGRATIO;
    }

    /**
     * Retrieve the violation threshold
     * 
     * @return The violationThreshold
     */
    public int getViolationThreshold() {
        return violationThreshold;
    }

    /**
     * Set the violation threshold
     * 
     * @param violationThreshold The ViolationThreshold
     */
    public void setViolationThreshold(int violationThreshold) {
        this.violationThreshold = violationThreshold;
    }

    /**
     * Retrieve the image difference threshold
     * 
     * @return the imgDiffThreshold
     */
    public int getImgDiffThreshold() {
        return ImgDiffThreshold;
    }

    /**
     * Set the image difference threshold
     * 
     * @param imgDiffThreshold the imgDiffThreshold to set
     */
    public void setImgDiffThreshold(int imgDiffThreshold) {
        ImgDiffThreshold = imgDiffThreshold;
    }


}

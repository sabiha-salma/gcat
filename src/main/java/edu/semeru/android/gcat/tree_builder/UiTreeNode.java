/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.tree_builder;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.nodes.Element;


/**
 * This class represents a node in an UI tree.
 *
 * @author KevinMoran, Carlos modified by Boyang
 */
public class UiTreeNode extends BasicTreeNode {

    private static final Pattern BOUNDS_PATTERN = Pattern
            .compile("\\[-?(\\d+),-?(\\d+)\\]\\[-?(\\d+),-?(\\d+)\\]");
    // use LinkedHashMap to preserve the order of the attributes
    private final Map<String, String> mAttributes = new LinkedHashMap<String, String>();
    private String mDisplayName = "ShouldNotSeeMe";
    private Object[] mCachedAttributesArray;

    private String id;
    private Position pos;
    private Size size;
    private String name, type;
    private String elementFile;  //file name for the element
    private Element element;   // xml element in the res folder

    /**
     * Constructor.
     * 
     */
    public UiTreeNode() {
    }

    /**
     * Constructor.
     * 
     * @param x x coordinate of top left corner of UI component
     * @param y y coordinate of top left corner of UI component
     * @param width width of UI component
     * @param height height of UI component
     */
    public UiTreeNode(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Retrieve file name for element.
     * 
     * @return file name for element
     */
    public String getElementFile() {
        return elementFile;
    }

    /**
     * Set file name for element.
     * 
     * @param file file name for element
     */
    public void setElementFile(String file) {
        this.elementFile = file;
    }

    /**
     * Retrieve xml element string.
     * 
     * @return string of xml element
     */
    public String getElementString() {
        if (element == null) {
            return "";
        } else {
            return element.toString();
        }
    }

    /**
     * Retrieve element object.
     * 
     * @return element object
     */
    public Element getElement() {
        return element;
    }

    /**
     * Set element object.
     * 
     * @param element element object
     */
    public void setElement(Element element) {
        this.element = element;
    }

    /**
     * Retrieve id of element.
     * 
     * @return id of element
     */
    public String getId() {
        return id;
    }

    /**
     * Retrieve name of element.
     * 
     * @return name of element
     */
    public String getName() {
        return name;
    }
    

    /**
     * Add attribute to uiTreeNode
     * @param key attribute, ie "class, text, index, bounds"
     * @param value attribute value
     */
    public void addAtrribute(String key, String value) {
        mAttributes.put(key, value);
        updateDisplayName();
        if ("bounds".equals(key)) {
            updateBounds(value);
        }
    }

    /**
     * Builds the display name based on attributes of the node
     */
    private void updateDisplayName() {
        String className = mAttributes.get("class");
        if (className == null) {
            return;
        }
        String text = mAttributes.get("text");
        if (text == null) {
            return;
        }
        String contentDescription = mAttributes.get("content-desc");
        if (contentDescription == null) {
            return;
        }
        String index = mAttributes.get("index");
        if (index == null) {
            return;
        }
        String bounds = mAttributes.get("bounds");
        if (bounds == null) {
            return;
        }
        // shorten the standard class names, otherwise it takes up too much space on UI
        //		className = className.replace("android.widget.", "");
        //		className = className.replace("android.view.", "");
        className = className.substring(className.lastIndexOf(".") + 1, className.length());
        type = className;
        StringBuilder builder = new StringBuilder();
        // Index
        builder.append('(');
        builder.append(index);
        builder.append(") ");
        // Class name
        builder.append(className);
        if (!text.isEmpty()) {
            builder.append(':');
            builder.append(text);
        }
        // Content description
        if (!contentDescription.isEmpty()) {
            builder.append(" {");
            builder.append(contentDescription);
            builder.append('}');
        }
        // Bounds
        builder.append(' ');
        builder.append(bounds);
        mDisplayName = builder.toString();
        //this.pos = boundToStartPos(bounds);

        id = mAttributes.get("resource-id");
        if (id == null) {
            return;
        }

        name = mAttributes.get("text");
        if (id == null) {
            return;
        }

    }

    private void updateBounds(String bounds) {
        Matcher m = BOUNDS_PATTERN.matcher(bounds);
        if (m.matches()) {
            x = Integer.parseInt(m.group(1));
            y = Integer.parseInt(m.group(2));
            width = Integer.parseInt(m.group(3)) - x;
            height = Integer.parseInt(m.group(4)) - y;
            this.pos = new Position(x, y);
            this.size = new Size(width, height);
            mHasBounds = true;
        } else {
            throw new RuntimeException("Invalid bounds: " + bounds);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Object[] getAttributesArray() {
        // this approach means we do not handle the situation where an attribute is added
        // after this function is first called. This is currently not a concern because the
        // tree is supposed to be readonly
        if (mCachedAttributesArray == null) {
            mCachedAttributesArray = new Object[mAttributes.size()];
            int i = 0;
            for (String attr : mAttributes.keySet()) {
                mCachedAttributesArray[i++] = new AttributePair(attr, mAttributes.get(attr));
            }
        }
        return mCachedAttributesArray;
    }

    /**
     *
     * @return
     */
    public String printOutGUICheckerFormat() {
        return "[id, " + id + "]"
                + "[start position, " + pos + "]"
                + "[rectangle, " + size + "]"
                + "[name, " + name + "] ";
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param key
     * @return
     */
    public String getAttribute(String key) {
		return mAttributes.get(key);
	}
 /*public void setType(String type) {
		this.type = type;
	}*/
 /*public void setName(String name) {
		this.name = name;
	}*/

    /**
     *
     * @return
     */
    public Map<String, String> getAttributes() {
		return Collections.unmodifiableMap(mAttributes);
        }
        /*public Position getPos() {
		return pos;
	}

	public Size getSize() {
		return size;
	}*/

 /*@Override
	public String toString() {
		//return mDisplayName;
		return "(" + x + "," + y + "," + width + "," + height + "); " +  name +"   type: " +  type;
	}*/
}

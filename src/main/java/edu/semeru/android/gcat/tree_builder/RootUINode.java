/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.tree_builder;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import edu.semeru.android.gcat.helpers.ImagesHelper;
import edu.semeru.android.gcat.matching.MatchingAnalysis;

/**
 * This class represents a root node in a UI tree
 *
 * @author KevinMoran, Carlos, modified by Boyang
 */
public class RootUINode extends BasicTreeNode {

    private final String mWindowName;
    private final String mRotation;
    private Object[] mCachedAttributesArray;
    private PrintWriter writer;
    private ArrayList<UiTreeNode> leafNodes = new ArrayList<UiTreeNode>();
    
    /**
     * File paths for cropped images from the previous and new commit and
     * black and white PID output
     */
    private String prevImgFolderPath = "html" + File.separator + "res" 
            + File.separator + "images" + File.separator +"prev";
    private String newImgFolderPath = "html" + File.separator + "res" 
            + File.separator + "images" + File.separator + "new";
    private String pidBWPath = "html" + File.separator + "res" + File.separator
            + "pidbw";

    /**
     * Constructor.
     */
    public RootUINode() {
        mWindowName = "";
        mRotation = "";
    }

    /**
     * Constructor.
     * 
     * @param windowName
     * @param rotation
     */
    public RootUINode(String windowName, String rotation) {
        mWindowName = windowName;
        mRotation = rotation;
    }

    /**
     * Retrieve list of all LeafNodes
     * 
     * @return list of all leaf nodes
     */
    public ArrayList<UiTreeNode> getLeafNodes() {
        leafNodes.clear();                                          
        for (BasicTreeNode node : this.mChildren) {
            getLeafNodesHelper(node);                         
        }
        return leafNodes;
    }

    /**
     * A helper function to get leaf nodes
     *
     * @param node current node
     */
    private void getLeafNodesHelper(BasicTreeNode node) {
        if (node.mChildren.size() == 0 && node.merged != true) {
            leafNodes.add((UiTreeNode) node);
        }
        for (BasicTreeNode child : node.mChildren) {
            getLeafNodesHelper(child);
        }
    }

    @Override
    public String toString() {
        return mWindowName + " - " + mRotation;
    }

    /**
     * Return attribute array.
     * 
     * @return attribute array
     */
    @Override
    public Object[] getAttributesArray() {
        if (mCachedAttributesArray == null) {
            mCachedAttributesArray = new Object[]{new AttributePair("window-name", mWindowName),
                new AttributePair("rotation", mRotation)};
        }
        return mCachedAttributesArray;
    }

    /**
     * Transform the tree based on heuristic rules
     */
    public void transform() {
        //Delete node if it has same size as its parent node
        removeRedundantNode();                                                
    }

    /**
     * Assign attributes to element from xml
     * 
     * @param mapIDElem hashmap value to assign
     */
    public void assignRes(HashMap<String, ElementDocfile> mapIDElem) {
        ArrayList<UiTreeNode> leafNodes = this.getLeafNodes();
        for (UiTreeNode uiNode : leafNodes) {
            int index = uiNode.getId().indexOf(":id/");
            if (index == -1) {
                continue;
            }
            String nodeID = uiNode.getId().substring(index + 4);
            ElementDocfile elemDoc = mapIDElem.get("@id/" + nodeID);
            if (elemDoc != null) {
                uiNode.setElement(elemDoc.getEle());
                uiNode.setElementFile(elemDoc.getDoc());
            }
        }
    }

    /**
     * Array of redundant nodes near root of commit tree
     */
    private ArrayList<BasicTreeNode> redundantNodes = new ArrayList<BasicTreeNode>();

    /**
     * Delete node if it has same size as its only child
     */
    private void removeRedundantNode() {
        for (BasicTreeNode child : this.mChildren) {
            detectRedundantNodeHelper(child);
        }

        // Remove nodes
        for (BasicTreeNode node : redundantNodes) {
            BasicTreeNode parent = node.getParent();
            int index = parent.mChildren.indexOf(node);
            parent.mChildren.remove(node);
            if (index == -1) {
                continue;
            }
            parent.mChildren.addAll(index, node.mChildren);
            for (BasicTreeNode child : node.mChildren) {
                child.mParent = parent;
            }
        }
    }

    /**
     * Helper - Delete node if it has same size as its only child
     */
    private void detectRedundantNodeHelper(BasicTreeNode node) {

        if (node.mChildren.size() == 1) {
            redundantNodes.add(node);
        }

        if (node.x == 0 && node.y == 0
                && node.width == 0 && node.height == 0) {
            redundantNodes.add(node);
        }

        int[][] ignoredNodes = ConstantSettings.getInstance().getIgnoredCompDesign();

        if (ignoredNodes != null) {
            for (int[] iNode : ignoredNodes) {
                BasicTreeNode tempNode = new BasicTreeNode(iNode[0], iNode[1], iNode[2], iNode[3]);
                if (tempNode.contains(node) || tempNode.equals(node)) {
                    redundantNodes.add(node);
                }
            }
        }
        for (BasicTreeNode child : node.mChildren) {
            detectRedundantNodeHelper(child);
        }
    }

    /**
     * Crop all nodes in the implementation from the input file
     * 
     * @param nodeType "prev" or "new" specifies if old or new commit
     */
    public void cropScreen(String nodeType) {
        File dir = null;
        //Make sure the folder is exist
        if (nodeType == "prev") {
            dir = new File(prevImgFolderPath);
            if (!dir.exists()) {
                dir.mkdir();
            }
        } else if (nodeType == "new") {
            dir = new File(newImgFolderPath);
            if (!dir.exists()) {
                dir.mkdir();
            }
        }

        dir = new File(pidBWPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        for (BasicTreeNode child : this.mChildren) {
            cropScreenHelper(child, nodeType);
        }
    }

    /**
     * Helper - crop all nodes in the implementation from the input file
     * 
     */
    private void cropScreenHelper(BasicTreeNode node, String nodeType) {
        File dir = null;
        //Crop
        try {
            if (nodeType == "prev") {
                dir = new File(prevImgFolderPath);
                ImagesHelper.cropImageAndSave(MatchingAnalysis.prevImageJpg, dir + File.separator + node.hashCode() + ".jpg",
                node.x, node.y, node.width, node.height, "jpg", node);
                
            } else if (nodeType == "new") {
                dir = new File(newImgFolderPath);
                ImagesHelper.cropImageAndSave(MatchingAnalysis.newImageJpg, dir + File.separator + node.hashCode() + ".jpg",
                node.x, node.y, node.width, node.height, "jpg", node);
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        for (BasicTreeNode child : node.mChildren) {                               
            cropScreenHelper(child, nodeType);
        }
    }

    /**
     * Compare two RootUINodes
     * @param comparedNode node to compare
     * @return boolean representing if nodes are equal
     */
    public boolean equals(RootUINode comparedNode) {
        return ((comparedNode.mWindowName == this.mWindowName) && (comparedNode.mRotation == this.mRotation));
    }
    
    
}

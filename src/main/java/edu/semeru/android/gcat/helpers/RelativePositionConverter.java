/*******************************************************************************
 * Copyright (c) 2016, SEMERU
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/

package edu.semeru.android.gcat.helpers;


import edu.semeru.android.gcat.tree_builder.ConstantSettings;
import edu.semeru.android.gcat.tree_builder.UiTreeNode;

/**
 *
 * @author georgewpurnell
 */
public class RelativePositionConverter {

    private int fromX;
    private int fromY;
    private int toX;
    private int toY;
    private int fromWidth;
    private int fromHeight;
    private int toWidth;
    private int toHeight;

    private float ratioWidth;
    private float ratioHeight;

    /**
     * Calculates the relative position between two nodes based on their board sizes.
     * @param boardFrom first node's board
     * @param boardTo second node's board
     */
    public RelativePositionConverter(int[] boardFrom, int[] boardTo) {
        fromX = boardFrom[0];
        fromY = boardFrom[1];
        toX = boardTo[0];
        toY = boardTo[1];

        fromWidth = boardFrom[2];
        fromHeight = boardFrom[3];
        toWidth = boardTo[2];
        toHeight = boardTo[3];

        ratioWidth = (float) toWidth / fromWidth;
        ratioHeight = (float) toHeight / fromHeight;
    }

    /**
     * Calculate distance between two nodes
     * @param nodeFrom first node
     * @param nodeTo second node
     * @return distance between given nodes
     */
    public float heuristicDistance(UiTreeNode nodeFrom, UiTreeNode nodeTo) {
        float rFromX = nodeFrom.getX() * ratioWidth;
        float rFromY = nodeFrom.getY() * ratioHeight;
        float rFromWidth = nodeFrom.getWidth() * ratioWidth;
        float rFromHeight = nodeFrom.getHeight() * ratioHeight;
        float dis = Math.abs(nodeTo.getX() - rFromX);
        dis += Math.abs(nodeTo.getY() - rFromY);
        dis += Math.abs(nodeTo.getWidth() - rFromWidth);
        dis += Math.abs(nodeTo.getHeight() - rFromHeight);
        if (isTextMatch(nodeFrom, nodeTo)) {
            int threshold = ConstantSettings.getInstance().getThresholdMatchDistance();
            //decrease the distance by 1/2 of the threshold if we have a text matching
            //dis -=  threshold/2;
            dis *= 0.1;
        }
        return dis;
    }

    /**
     * Convert x to relative position on other board
     * @param fromX x of current board
     * @return relative x of compared board
     */
    public float getConvertX(int fromX) {
        return fromX * ratioWidth;
    }

    /**
     * Convert y to relative position on other board
     * @param fromY y of current board
     * @return relative y of compared board
     */
    public float getConvertY(int fromY) {
        return fromY * ratioHeight;
    }

    /**
     * Convert width to relative width on other board
     * @param fromWidth width of current board
     * @return relative width of compared board
     */
    public float getConvertWidth(int fromWidth) {
        return fromWidth * ratioWidth;
    }

    /**
     * Convert height to relative height of other board
     * @param fromHeight height of current board
     * @return relative height of compared board
     */
    public float getConvertHeight(int fromHeight) {
        return fromHeight * ratioHeight;
    }

    /**
     * Check if the two nodes are matched based on text
     *
     * @param dsNode old commit node
     * @param uiNode new commit node
     * @return true if text for both nodes is the same
     */
    private boolean isTextMatch(UiTreeNode dsNode, UiTreeNode uiNode) {
        String dsText = dsNode.getName();
        String uiText = uiNode.getName();
        return dsText.equals(uiText);
    }

}

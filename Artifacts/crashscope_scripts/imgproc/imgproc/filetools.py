import os, sys, argparse
from shutil import copyfile

from imgproc.imageset import ImageSet
from PIL import Image

# For the progress bar
import time
import progressbar


def get_filenames_of_extension(directory, extension):
    """Iterate over the filenames in a directory with a specific extension."""
    filenames = (f for f in os.listdir(directory) if f.split(".")[-1] == extension)
    bar = progressbar.ProgressBar()
    for filename in bar(filenames):
        yield filename


def remove_duplicates(directory):
    """!!NOT ROBUST, it removes SOME duplicates, but some are not detected because
    of minute, inperceptable differences."""
    image_set = ImageSet()

    for filename in get_filenames_of_extension(directory, "png"):
        image = Image.open(directory + os.path.sep + filename)
        if not image_set.add(image):
            # Delete the image if it is a duplicate.
            os.remove(directory + os.path.sep + filename)

    print("| Operation complete. %d unique images identified." % len(image_set))


def match_screens(dir1, dir2):
    """Looks for IDENTICAL matches between two directories. Does not catch them all
    because of minute, inperceptable differences in the images (I think that's the reason)."""
    matches = []

    print("Building reference set...")
    image_set = ImageSet()
    for filename in get_filenames_of_extension(dir1, "png"):
        image = Image.open(dir1 + "/" + filename)
        image.info["filename"] = filename
        image_set.add(image)

    print("Checking new values...")
    for filename in get_filenames_of_extension(dir2, "png"):
        image = Image.open(dir2 + "/" + filename)
        if image in image_set:
            match_fn = image_set.find_match(image)
            if match_fn:
                matches.append((filename, match_fn.info["filename"]))

    print("%d Matches found." % len(matches))

    return matches


def extract_version_numbers(directory):
    """ Parses out the version numbers of the files in a directory"""
    versions = []
    for filename in os.listdir(directory):
        splt = filename.split("_")
        if len(splt) > 1:
            version = splt[-2]
            if version not in versions:
                versions.append(version)

    if len(versions) != 2:
        raise RuntimeError("There are not two versions present: %s" % str(versions))
    return versions


def make_dirs(directory, names):
    for name in names:
        if not os.path.exists(directory + os.path.sep + name):
            os.makedirs(directory + os.path.sep + name)


def sort_by_version(directory):
    """ Iterates over the files in a directory, sorting the files into two new
    directories that correspond to their version numbers. """
    versions = extract_version_numbers(directory)
    make_dirs(directory, versions)

    for filename in get_filenames_of_extension(directory, "png"):
        if versions[0] in filename:
            copyfile(os.path.join(directory, filename), 
                    os.path.join(directory, versions[0], filename))
        elif versions[1] in filename:
            copyfile(os.path.join(directory, filename), 
                    os.path.join(directory, versions[1], filename))
        else:
            msg = "File %s does not contain a version number: %s" % (filename, str(versions))
            raise RuntimeError(msg)


def retrieve_xml(img_directory, xml_directory):
    """ Determines the step number and version number of all the files in a directory, 
    and copies the corresponding xml files into that directory."""
    version = img_directory.split(os.path.sep)[-1]
    step_numbers = set()
    for f in get_filenames_of_extension(img_directory, "png"):
        step_number = f.split("gnucash")[-1].split(".")[0]
        print(step_number)
        step_numbers.add(step_number)

    for f in get_filenames_of_extension(xml_directory, "xml"):
        if version in f and f.split("-")[-1].split(".")[0] in step_numbers:
            copyfile(os.path.join(xml_directory, f), os.path.join(img_directory, f))

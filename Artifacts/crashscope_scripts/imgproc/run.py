#! /usr/bin/env python3
from imgproc.filetools import *

data_root = os.getcwd()
screens = data_root + os.path.sep + "screenshots"

# Extracts the version numbers from the filenames of the images in screenshots.
print("Extracting version numbers...")
versions = extract_version_numbers(screens)

# Creates two new folders in the screenshots folder corresponding to the version numbers,
# and sorts all the screens in screenshots into their respective folder.
print("Sorting by versions...")
sort_by_version(screens)

# Paths to the new folders.
version0_path = screens + os.path.sep + versions[0]
version1_path = screens + os.path.sep + versions[1]

# Remove duplicates from both new folders.
print("Removing duplicates from version %s..." % versions[0])
remove_duplicates(version0_path)
print("Removing duplicates from version %s..." % versions[1])
remove_duplicates(version1_path)

# Match the screens between versions that are identical matches.
print("Attempting to match...")
matches = match_screens(version0_path, version1_path)

#print(version0_path, data_root)
#retrieve_xml(version0_path, data_root)
#retrieve_xml(version1_path, data_root)


# Write all the matches (represented as a list of tuples of filenames) to a file.
with open("matches.txt", "w") as output:
    string = ""
    for match in matches:
        string += ",".join(match) + "\n"
    output.write(string)


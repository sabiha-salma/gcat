from PIL import Image
from scipy.optimize import linear_sum_assignment
import imagehash
import numpy as np
import os

# Change these values 
SCREEN_PATH1 = "/home/jhoskins/projs/lsh/cs-data/screenshots/1.1.10"
SCREEN_PATH2 = "/home/jhoskins/projs/lsh/cs-data/screenshots/1.1.13"

# These become the dimensions of the cost matrix
screen_count1 = len(list(os.listdir(SCREEN_PATH1)))
screen_count2 = len(list(os.listdir(SCREEN_PATH2)))

# Build the cost matrix
cost = np.zeros((screen_count1, screen_count2), dtype=int)
for i, filename1 in enumerate(os.listdir(SCREEN_PATH1)):
    for j, filename2 in enumerate(os.listdir(SCREEN_PATH2)):
        hsh1 = imagehash.dhash(Image.open(SCREEN_PATH1 + os.path.sep + filename1))
        hsh2 = imagehash.dhash(Image.open(SCREEN_PATH2 + os.path.sep + filename2))
        hamming_distance = int(hsh1 - hsh2)
        cost[i, j] = hamming_distance

# Run the optimization algorithm
row_ind, col_ind = linear_sum_assignment(cost)

# Turn the mapping of row indicies -> col indicies into a mapping of
# 1.1.10 filenames -> 1.1.13 filenames
files1 = list(os.listdir(SCREEN_PATH1))
files2 = list(os.listdir(SCREEN_PATH2))
matching = []
for x, y in zip(row_ind, col_ind):
    matching.append((files1[x], files2[y]))

# Write the matches to a file
with open("matches.txt", "w") as f:
    s = ""
    for match in matching:
        s += ",".join(match) + "\n"
    f.write(s)

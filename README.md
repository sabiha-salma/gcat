# Android GUI Change Analysis Tool (G-CAT)

## Project Summary

Mobile applications evolve at a rapid pace. During the development lifecycle of a mobile app, new features are implemented based on user demand, fixes are applied to existing bugs, and underlying platforms and APIs are updated, which all drive the evolution of an underlying codebase. To support this rapid development process, mobile software engineers need automated support for documenting and understanding the changes an app undergoes. 

Because mobile applications are heavily GUI-driven, much of the functionality is tied to code related to the user interface. Therefore, changes to the user interface are of paramount importance for developers to understand and document in order to maintain a detailed working knowledge of an evolving codebase. 

To help aid in the comprehension of GUI changes in evolving mobile apps we have developed GCat which is capable of automatically detecting, summarizing, and documenting GUI-changes in mobile apps.

## Use Cases

This tool is meant for comparing GUI changes between two commits. It can be used to:

* Optimize front-end development when working in a large group of developers
* Simplify GUI backtracking by highlighting where changes have occurred
* Quantify GUI changes for client approval and comparison
* Allow user to closely model other application's GUI

## Getting Started

### Prerequisites

Gcat accepts two screenshots, and two XML [uiautomator](https://stuff.mit.edu/afs/sipb/project/android/docs/tools/help/uiautomator/index.html) files as input.
We recommend using The Android Debug Bridge, or [`adb`](https://developer.android.com/studio/command-line/adb.html) to capture screenshots and UI-dumps from a target app screen running on an Android device or emulator. You can find instructions on installing `adb` on your computer [here]().

We have developed GCat to run on macOS, Linux, and Windows. However, the development, documentation, and testing of GCat is primarily conducted on Linux and macOS, and thus these are the recommended platforms for this tool. 

### Installing and Running the GCat Binaries

We developed GCat using GtiLab's CI system, so that every commit to the public-master branch of GCat is autaomtically tested and built to help ensure the quality of the tool. While we strive to keep the public-master branch as stable as possible, there may unexpected or undocumented changes as we continue development on the tool. Therefore users can also utilize one our stable [tagged releases](https://gitlab.com/SEMERU-Code-Public/Android/gcat/tags).

* Download the [latest build](https://gitlab.com/SEMERU-Code-Public/Android/GCat/-/jobs/artifacts/public-master/download?job=BuildGCat) of the public-master branch, or one of the [stable releases](https://gitlab.com/SEMERU-Code-Public/Android/gcat/tags)
* Unzip the downloaded directory
* Depending on your operating system, you may need to give the following files permission to run:
    * `GCAT/libs/pid-linux/perceptualdiff`
    * `GCAT/libs/pid-windows/perceptualdiff.exe`
    * `GCAT/libs/pid-mac/perceptualdiff`
* Move the `Gcat.jar` executable out of the `target` folder and into the root of the downloaded directory. If you are on a Unix-Like system, you can run the following commands from the root of the downloaded folder:

```shell
$ mv /target/GCat.jar ..
$ rm -r /target
```
* Now you are ready to run the GCat executable as outlined later in the README.

### Importing and Compiling the Project in Eclipse

GCat was developed using the Eclipse IDE and its dependencies and tests are managed using Maven. We include all the necessary files to import the project directly into Eclipse. 

* First clone the GCat repo on your local machine.
* In Eclipse, simply go to File --> Import, and then select the "Existing Java Projects into Workspace" option.
* Click the "Next" button, and then under the "Select root directory" option on the next screen browse to the root of of the cloned GCat repo.
* Click the "Finish" Button to complete the import process.
* The project will now automatically compile using Eclipse.

### Compiling GCat from the Command Line

You can also compile and test GCat from the command line using maven. In order to to do this, you must have maven installed on your machine and in your `$PATH`. 

To compile the project into a `.jar` without running any tests:

* Clone the GCat repo to your local machine
* Open a bash shell in the root of the cloned repository and enter the following command:

```shell
$ mvn -Dmaven.test.skip=true package
```

To run all tests associated with the GCat project and compile the project into a `.jar`:

* Clone the GCat repo to your local machine
* Open a bash shell in the root of the cloned repository and enter the following command:

```shell
$ mvn package
```

### Capturing Input Files from a Device or Emulator

In order to use GCat, you must first capture a set of four input files from an app running on a device or emulator connected to your computer. To do this, make sure you have `adb` installed on your computer and connect the device or emulator to your computer using `adb`.

To capture screenshots of the two GUIs you want to compare (more info [here](https://developer.android.com/studio/command-line/adb.html#screencap)) you can run the following command:

```shell
$ adb shell screencap /sdcard/screenshot.png
$ adb pull /sdcard/screenshot1.png screenshot1.png
```

To get the uiautomator dumps of the two GUIs you want to compare (more info [here](https://stuff.mit.edu/afs/sipb/project/android/docs/tools/help/uiautomator/index.html)) you can run the following commands:

```shell
$ adb shell uiautomator dump /sdcard/dump1.xml
$ adb pull /sdcard/dump1.xml dump1.xml
```

You will need to run this set of commands twice, once for each screen that you wish to analyze, which should result in a screenshot and UI-dump for each version of the app that you want to compare.


## Usage

* Run the following command in the GCAT folder, making sure to use __absolute paths__:

```shell
$ java -jar GCAT.jar <screenshotPath1> <xmlDumpPath1> <screenshotPath2> <xmlDumpPath2>
```

GCAT will be run with default settings. A summary and itemized list of changes will be output to the screen, and a full HTML report of the changes will be generated in the newly created folder, `Outputs/html/Full-Report.html`.

Open this file in your web browser to see the results of the GUI differencing algorithm.

### Parameters

To streamline input for the command line, drag and drop the appropriate
files into the command line window. This should copy the absolute path.

The input files required are:

* Previous commit `.png` file containing screenshot of entire screen - referenced in instructions as the absolute path `imgPath1`
* Previous commit `.xml` file generated using UiAutomator - referenced in instructions as absolute path `uidumpPath1`
* New commit `.png` file containing screenshot of entire screen - referenced in instructions as absolute path `imgPath2`
* New commit `.xml` file generated using UiAutomator - referenced in instructions as absolute path `uidumpPath2`

### Example Input Files

For example files for each input see the following:

* [newCommitImage](Artifacts/test-files/test-screenshot-2.png)
* [newCommitXml](Artifacts/test-files/test-uiautomator-2.xml)
* [oldCommitImage](Artifacts/test-files/test-screenshot-1.png)
* [oldCommitXml](Artifacts/test-files/test-uiautomator-1.xml)

### Usage Flags

The following flags can be added to your shell command to modify the output as follows:

| Command | Parameter | Default | Effect |
|---|---|---| --- |
|```--htmlreport```| None | None | Opens a visual summary of the GCAT output automatically in your default browser when the analysis is complete. |
|```--violationthreshold```| Integer | 15 | Sets the value that GCAT will use to determine when a visual difference should be counted as an image difference.  | 
|```--imgDiffthreshold```| Integer | 20 | Sets the value that GCAT will use to determine when objects have moved versus when they have been deleted. |
|```--ignoredareas```| String of the form, "x,y,w,h:x2,y2,w2,h2" where x is the x coordinate of the area, y is the y coordinate of the area, w is width, and h is height.| "0,0,1440,100:0,2372,1440,188" | Set areas on the screenshot that GCAT will ignore when scanning for changes. |

## Output

If the tool is run multiple times, the output files are overwritten with the most recent input parameters.

An html page is created with:

* A natural language summary of the changes between the two GUIs
* Detailed information about each change
* Links to view each GUI hierarchy, as well as their maximum common tree.

The directory containing all HTML files can be found at the absolute path: `Android-Summarizing-GUI-Changes/Code/guigit/html`

Explanation of Files:

* `../commonTree.htm` - displays a visual tree hierarchy of the maximum spanning common tree
* `../tree.html` - displays the tree hierarchies generated by each commit input 
* `../summaryTemplateDefault.html` - summary of GUI changes in natural language and detailed changes with images
* `../Landing Page/landingPage.html` - navigation between separate HTML files


The `tree.html hierarchy` is color-coded according to the following schema in order to highlight differences:

* White = Nodes in the tree are exactly the same 
* Grey  = Layout changes near root of trees
* Red   = Tree nodes are different in value or location in the tree 

## Our Team

### Advisors

* [Kevin Moran](http://www.kpmoran.com)
* [Denys Poshyvanyk](http://www.cs.wm.edu/~denys/index.html)

### Graduate Student Contributors

* [Cody Watson](http://www.cs.wm.edu/~cawatson/)
* [George Purnell](http://georgewpurnell.com/about-me/)

### Undergraduate Contributors

* Shuning Chen
* Louisa Doyle 
* Claudia Estes 
* John Hoskins 

## Citation

If you utilize GCat in your academic work, we kindly ask that you cite our ASE'18 paper:

> Moran, K., Watson, C., Hoskins, J., Purnell, G., and Poshyvanyk, D., “Detecting and Summarizing GUI Changes in Evolving Mobile Apps”, in Proceedings of 33rd IEEE/ACM International Conference on Automated Software Engineering (ASE'18), Montpellier, France, September 3-7, 2018, pp. 543-553